## Book Questions

#### R16 Suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a message to Bob, who accesses his mail from his mail server using IMAP. Discuss how the message gets from Alice’s host to Bob’s host. Be sure to list the series of application-layer protocols that are used to move the message between the two hosts.

Once Alice composes her email and sends it, it uses SMTP. Once it moves through the layers of the internet, Bob handles the data using IMAP.

#### R18 What is the HOL blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?

The head of line blocking is that large items at the top of an HTML page will hold up smaller items behind. HTTP/1.1 opens multiple TCP connections so that different parts of the page simultaniously. The problem with this is that, instead of having the fastest transmision, the bandwidth is broken up to each TCP connection. HTTP/2 breaks down all of the HTTP messages into smaller frames. The smaller peices of the webpage can then be interleaved with the large packet. 

#### R24 CDNs typically adopt one of two different server placement philosophies. Name and briefly describe them.

**Enter Deep** is to enter deep into the CDN structure by placing server clusters all over the world. 
The goal is to create geographicaly useful servers. They are designe d to improving user-perceived delay and throughput by decreasing the number of links and routers between the end user and the CDN server from which it receives content. Maintaining clusters of serversw around the world is expensive but can be worth it the user experience is positive enough.
**Bring Home** is from the perspective of the ISPs, where they are bring their servers to a home location and making the user wait on the delays but saving resources on distribution and server maintainence. 

#### P16 How does SMTP mark the end of a message body? How about HTTP? Can HTTP use the same method as SMTP to mark the end of a message body? Explain.

SMTP end a message body with a period (.). HTTP on the other hand uses \r\n to deliminate. They are different and would need a different method to be ended. 
 
#### P18(a and b only), 
##### a. What is a whois database?

The WHOIS database holds a broad range of IP addresses and domain names and is used for querying information about domains and how they are set up.

##### b. Use various whois databases on the Internet to obtain the names of two DNS servers. Indicate which whois databases you used.

I used whois.com.  MLB.com has a bunch of servers but the biggest seems like: ns-cloud-b1.googledomains.com, ns-cloud-b2.googledomains.com, ns-cloud-b3.googledomains.com, ns-cloud-b4.googledomains.com

#### P20 Consider the scenarios illustrated in Figures 2.12 and 2.13. Assume the rate of the institutional network is Rl and that of the bottleneck link is Rb. Supposem there are N clients requesting a file of size L with HTTP at the same time. For what values of Rl would the file transfer takes less time when a proxy is installed at the institutional network? (Assume the RTT between a client and any other host in the institutional network is negligible.)


#### P21 Suppose that your department has a local DNS server for all computers in the department. You are an ordinary user (i.e., not a network/system administrator). Can you determine if an external Web site was likely accessed from a computer in your department a couple of seconds ago? Explain.