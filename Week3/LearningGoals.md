## Learning Goals

#### I can explain how a the DNS system uses local, authoritative, TLD, and root servers to translate an IP address into a hostname.

Domain Name System refers to the overarching system that resolves domain names to IP addresses and vice versa. Local servers are used to copy distribute to coralating Domain Name/IP Address data throughout the network to reduce the amount of space that has to be spanned. The Authoritative DNS is where the actual resolution can happen. The root DNS servers are the highest level of the DNS hierarchy. The root holds the path to the Authoritative DNSs. TLD servers are the Top Level domain servers. They dirrect to the 

#### I can explain the role of each DNS record type.

**Type=A** does the standard hostname to IP conversions. This means that the hostname was received and found and an IP address was returned.  

**Type=NS** means that an authoritative route to the IP address has been found. It is described as an "almost there" response.

**Type=CNAME** means it has found a canonical hostname for the alias hostname. It is like a "oh did you mean this" type of response. This record can provide querying hosts the canonical name for a hostname.

If **Type=MX**, then Value is the canonical name of a mail server that has an alias hostname Name. This means that the mail server might have a simpler or more applicable alias to make it easier for users to get to the mail server. 

#### I can explain the role of the SMTP, IMAP, and POP protocols in the email system.

SMTP is the protocal used for all electronic mail. SMTP starts by creating a secure TCP connection on port 25 for the mail to be sent over. SMTP is a push protocal which means that it is unable to send a message as a pull protocal system like a server. This is where IMAP comes in. IMAP allows mail servers to handle folders. Points of presence are needed to properly and promtly route messages from clients to their desired location in the most efficient manner possible.

#### I know what each of the following tools are used for: nslookup, dig, whois

**nslookup** is a very handy program that sends a DNS request and displays the path that your request took to the resolution of your request.

**dig** has a lot of flexibility with all of its possible flags. In essence it is used to obtain information from 
    a DNS server with as verbose of responses as needed. 

**WHOIS** is used with a website to provide all the information about a website from the owner or IP Address to all pertinent flags a user might want to access. 