## Week 2 Learning Goals Responses

#### I can explain the HTTP message format, including the common fields.

HTTP messages can be either request or response messages which both have their own formats. Both formats require a lot of attention to detail as a missed space or return will deem the entire message void. Both message types have five lines all ending in a return and a "line feed." The last line also has to have return and line feed. 
**A request massage has a:** Request Line, Header Lines (3), Blank Line, and an Entity Body.
**A response has:** Status Line, Header Lines (3), Blank Line, and an Entity Body.


#### I can explain the difference between HTTP GET and POST requests and why you would use each.
    
POST is used for "filling out a form." The entity body will be made up of items that the user has inserted into the request. 

A GET request does not use the entity body and is much more focused on makeing a direct request. 

#### >I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.

Conditional GET requests are used to solve the problem of cached data becoming out dated. If an HTTP request is made to a CDN there is a chance that that CDNs data is not up to date. A conditional Get request can ensure that it is. A Conditional GET request needs to use the GET request method and include If-Modified-Since in the header. The Conditional GET gives the server a date after the "If-Modified-Since" and the server can then save bandwidth by sending a response with an empty entity body if no updates are needed. 

#### I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.

The status code is included in the Status line of the response message and provides an answer to how the request was received. 200 indicates everything works. 301 means that URL has been moved and will provide the forwarding address. 400 is a bad request. 404 is site not found. 

#### I can explain what cookies are used for and how they are implemented.

Cookies are made up of four elements. A cookie header line in the request and response messages, a data kept in the users browser, and a data base maintained at the website. Cookies are used to store and transmit data about a user in relation to a specific website. When a client first interacts with a Web server, that client has an ID created for them which is then included in the HTTP messages. This ID corrilates to the file that holds all the information about the user. 

#### I can explain the significance HTTP pipelining.
    
Pipelining can be thought of as HTTP requests making a hustle play as in sports. Instead of waiting for responses, requests are made back-to-back. This keeps the servers port open and cuts down lag time as the server does not need to wait for the response to get to the client and the next request to come back. By holding the requests in a queue, the server able crank out the responses.  

#### I can explain the significance of CDNs, and how they work.

CDNs are significant becuase they localize data. By routing requests to a CDN the entire network can feel smoother and more local. Private CDNs are maintained by companys to distribute their own information in an efficient manner. Third-Party CDNs are a seperate group that sell their space to people interested in improving connectivity, but are too small to maintain their own CDN. One method for implementing CDNs is to "enter deep," by essentially carpeting the world with CDNs. Entering Deep focuses on maintaining a large number of CDNs everywhere in the world. This can be difficult to maintain due to the sheer amount of CDNs that requires. The other implementation philosophy is the bring it home approach. This approach focuses on integrating CDNs closer with the Internet Exchange Points to keep a large area of effectivness by using the IXP connections. This generally requires less maintainence by using a more strategic approach to the implementation.    