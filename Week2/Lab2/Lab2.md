#### Question 1: Why are we focusing on the TCP server in this lab rather than the UDP server?

HTML is run over TCP

#### Question 2: Look carefully at Figure 2.9 (General format of an HTTP response message). Notice that there's a blank line between the header section and the body. And notice that the blank line is two characters: cr and lf. What are these characters and how do we represent them in our Python response string?

\r and \n

#### Question 3: When a client requests the "/index.html" file, where on your computer will your server look for that file?

In the same file that path that the server is being run from. 
 