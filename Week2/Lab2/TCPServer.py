#import socket module
from socket import *

# In order to terminate the program
import sys 

#prepare and bind a socket
#then prep for reception
serverSocket = socket(AF_INET,SOCK_STREAM)
serverPort = 19000
serverSocket.bind(('',serverPort))
serverSocket.listen(1)
print('The server is ready to receive')
content_type = "text/plain"

while True:
    #Make the connection
    connectionSocket, addr = serverSocket.accept()
    print(f"Accepted connection from {addr}")
    try:
        message = connectionSocket.recv(1024).decode()
        
        if message:
            if ".html" in message:
                print('1\n')
                response = f"HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n"
            elif ".js" in message:
                print('2\n')
                response = f"HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n"
            elif ".jpg" in message:
                print('3\n')
                response = f"HTTP/1.1 200 OK\r\nContent-Type: image/jpg"
            else:
                print('4\n')
                response = "HTTP/1.1 404 Not Found\r\n\r\n404 Not Found"
        
        filename = message.split()[1]
        try:
            f = open(filename[1:])
        except:
            print(filename + ' trouble')
        try:
            outputdata = f.read()
        except:
            print('bad read of outdata')


        #Send one HTTP header line into socket
        
        connectionSocket.send(response.encode())   
        
        #Send the content of the requested file to the client
        for i in range(0, len(outputdata)):
            connectionSocket.send(outputdata[i].encode())
        connectionSocket.send("\r\n".encode())

        connectionSocket.close()



    except IOError:
        raise IOError("404, can't find that file")

serverSocket.close()
sys.exit()

