## Book Question Answers:

#### R5, What information is used by a process running on one host to identify a process running on another host?

An IP Address is used.

#### R8, List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP or TCP (or both)  provides such a service.

**Reliable Data Transfer:** TCP uses a constant flow of data that can be trusted to deliver all the bytes in proper order. TCP also controls congestion in the network which is optimal for keeping the network "neat." UDP is the opposite. It can send messages in any order and sometimes incomplete.

**Throughput:** This is not garenteed by either UDP or TCP. UDP uses the fire hydrant method to deliver packets meaning that it lends itself much better to higher throughputs. Both can have a high ceiling for throughput given well built physical or Transport layers, however it is very difficult to optomize at the application layer. 

**Timing** is going to be much more difficult to maintain by the nature or of UDP. The unreliable connection can lead to packets being misplaced and lost. TCPs ability to send packets as a constant stream makes it much better suited for delivering data in proper order.   

**Security:** The TCP uses transport layer security to help address security. Encryption, data integrity, and authentication are added to TCP to make it a more reliable protocal when it comes to security. UDP has no such measures and is very vulnerable. 

#### R11, What does a stateless protocol mean? Is IMAP stateless? What about SMTP?

A stateless protocal means that there is no information about the user held in the protocal. In a stateless protocal, additional elements like cookies have to be added to hold data about the client. SMTP, HTTP, and IMAP are Stateless protocal.  

#### R12, How can websites keep track of users? Do they always need to use cookies?

Website can use any element or the http communications to collect data on the user. The server simply needs to record the information and find a way to attach that data to that user. This can be a tracking pixel or user input.  

#### R13, Describe how Web caching can reduce the delay in receiving a requested object. Will Web caching reduce the delay for all objects requested by a user or for only some of the objects? Why?

Web cacheing creates a much more accessable bank of data. While they do not hold the data of an entire server, they can be optimized toprovided the "Most likely needed" data. By storing this smaller amount of datain direct communication with clients is much faster. The downside is that they are not the entire server and can become outdated quiqkly. When a cache does not have the requested data it makes a call to the server and updates its local files.   

#### R14, Telnet into a Web server and send a multiline request message. Include in the request message the If-modified-since: header line to force a response message with the 304 Not Modified status code.

I am still unsure about this concept.  

#### R26. In Section 2.7, the UDP server described needed only one socket, whereas the TCP server needed two sockets. Why? If the TCP server were to support n simultaneous connections, each from a different client host, how many sockets would the TCP server need?

Becuase the TCP server estableshes a secure connection through the TCP "handshake," It uses an extra socket to listen for a knock. The single welcoming socket is the only extra socket element that TCP need so the TCP server needs n + 1 sockets.    