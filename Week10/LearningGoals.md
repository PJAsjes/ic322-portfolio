## Learning Goals

#### I can explain what autonomous systems are and their significance to the Internet.

Autonomous Systems maintian a the organization of the internet at the Network layer. Autonomous Systems are a smaller group of routers that run small and effiient protocols such as OSPF or RIP (among others). By keeping routers grouped together, effiiency within that group can be obtimized in different ways. The administrator of an AS can implement policies that work best for those routers. Perhaps one shortest path algorithm will work better than another for a specific AS simply by the nature of how many routers there are and how they are connected. There is also a level of security to an AS based on how much easier it is to block bad traffic at the gate routers. Due to the sheer size of the internet, it benefits from good organization at all levels and AS provide that for the Network layer.  

#### I can describe how the BGP protocol works as well as why and where it is used.

Autonomous Systems are a fantastic structural improvment for the internet however it raises the problem of how to connect Autonomous systems to eachother. This is solved by The Border Gateway Protocol, also known as the BGP. Certain routers in an AS fill the role of gateway routers that can connect to other ASs gateway routers to create connection. 
iBGP and eBGP break BGP into its internal and external functions. eBGP is used to send data from AS to AS. When a new route is being broadcast, iBGP is used to update the internal routers (non gateway routers) on the existence of the new path. 
Becuase BGP is run over a large number of ASs run by different groups, theyre policy for sending data can be unique. Some ASs want packets off their networks as quickly as possible. The is reffered to as Hot-potato routing. Other ASs keep packets for as long as possible which is reffered to as cold potato routing.     

#### I can explain what the ICMP protocol is used for, with concrete examples.

 Internet Control Message Protocol is the mothod used by receivers at the network layer to denote an error or lack of correct information in the correct order. It is a fairly simple but very useful element of the Network layer. 
 ICMP allows for routers to track the location of communication failure and problem solve around it. It is a convenient way for receivers to request transmission of needed information when it comes incomplete or out of order the first time. 
 If you want to know the route to a user or router, simply send incramenting packets with a time-to-live thats kills the packets at every hop on the way to the destination you want to know the router. ICMP will send back the information of each router on the way.    