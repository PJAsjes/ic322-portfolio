## Review Questions: 

#### R11, How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?

AS-PATH stands for Autonomous system path, which is literally the path of attonomous systems that that pack has gone through. 
NEXT-HOP is like the next clue is a treasure hunt. It does not show the whole path but it does get you to the next step. 

#### R13, True or false: When a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain.

False, local BGP policy will have the gateway router do whatever it wants with the path. If it already posseses that path it can send it in to confirm to all of its neighbors or it could throw the path out entirely. Good local policy whithin Autonomous Systems is foundational to BGP. 

#### R19 (also describe what they are used for), Names four different types of ICMP messages
    ICMP type 8 code 0 is an echo reply which is used for pinging a router. 
    Any ICMP type 3 is a response code that denotes that the message cannot reach its destination based on some element of the network being unreachaable. i.e. code 0: destination network unreachable. code 7: destination host unknown.  
    ICMP type 11 code 0 is the returning message when a messages time to live has expired. 
    ICMP  

#### R20, What two types of ICMP messages are received at the sending host executing the Traceroute program?

ICMP type 3 Code 3 (Dest. Port Unreachable) will denote the end of the traceroute.
ICMP type 11, Code 0 (TTL Expired) will show that hop in the route.

#### P14, 
#### P15, 
#### P19, 
#### P20
