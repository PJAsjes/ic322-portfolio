## Learning Goals

#### I can explain how two strangers are able to exchange secret keys in a public medium.

If two people want to send a message that noone else can read then they have to rely on the fact that certain math functuins can be very easily performed but require an emmense amount of computation to reverse. Both stranger needs to have a public and private key. If something is encrypted with the public key, it can only be decrypted with the private key. When a stranger wants to send a message to another stranger they simply need to encrypt the message with the intended receiver's public key and then only the receiver can decrypt it with their private key. 

#### I can walk through the TLS handshake and explain why each step is necessary.
First the client says hello, which initiates the conversation. The client hello contains the information about how the client can talk such as TLS version and any limitations the client might have. The server then says hello back and declares what protocol will be used for the handshake as far as encryption algorithms or any other random data. Depending on what encryption algorithm is being used, the key exchange takes place. The agreed keys are necessary for whatever encrption concept is being used. At this point the server can send its certificate and public key back to the client which can be checked against a certificate authority which prevents man in the middle attacks. The client then sends the master key which is created from a formula of the public and private keys. Finally, the server and client exchange a hash of all previous messages to ensure that no massages have been tampered with. At this point a secure connection has been formed and a private conversation can take place. 

#### I can explain how TLS prevents man-in-the-middle attacks.
Answered above 