### Book Questions

#### Chapter 6
###### R10, 
###### R11
###### P14, Consider three LANs interconnected by two routers, as shown in Figure 6.33.
###### a. Assign IP addresses to all of the interfaces. For Subnet 1 use addresses of the form 192.168.1.xxx; for Subnet 2 uses addresses of the form 192.168.2.xxx; and for Subnet 3 use addresses of the form 192.168.3.xxx.

###### b. Assign MAC addresses to all of the adapters. 

###### c. Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single-router example in Section 6.4.1.

###### d. Repeat (c), now assuming that the ARP table in the sending host is empty (and the other tables are up to date).



###### P15 Consider Figure 6.33. Now we replace the router between subnets 1 and 2
with a switch S1, and label the router between subnets 2 and 3 as R1.
a. Consider sending an IP datagram from Host E to Host F. Will Host E ask router
R1 to help forward the datagram? Why? In the Ethernet frame containing the
IP datagram, what are the source and destination IP and MAC addresses?
b. Suppose E would like to send an IP datagram to B, and assume that E’s
ARP cache does not contain B’s MAC address. Will E perform an ARP
query to find B’s MAC address? Why? In the Ethernet frame (containing
the IP datagram destined to B) that is delivered to router R1, what are the
source and destination IP and MAC addresses?
c. Suppose Host A would like to send an IP datagram to Host B, and neither A’s
ARP cache contains B’s MAC address nor does B’s ARP cache contain A’s
MAC address. Further suppose that the switch S1’s forwarding table contains
entries for Host B and router R1 only. Thus, A will broadcast an ARP request
message. What actions will switch S1 perform once it receives the ARP
request message? Will router R1 also receive this ARP request message? If so, will R1 forward the message to Subnet 3? Once Host B receives this ARP
request message, it will send back to Host A an ARP response message. But
will it send an ARP query message to ask for A’s MAC address? Why? What
will switch S1 do once it receives an ARP response message from Host B?


#### Chapter 7: 
###### R3, 
###### R4
###### P6, 
###### P7

Step takes account 