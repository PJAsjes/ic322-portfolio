### Learning Goals

##### I can describe the role a switch plays in a computer network.

Switches do the work of carrying messages between servers and clients. It is a Link Layer hardware used for connecting a frame that is looking for an IP address and needs to be converted to a MAC address. This is done using an ARP query to a switch table. The table is self learning which means that as requests come in, the switch adds the source of that request to its table. 

##### I can explain the problem ARP solves, how it solves the problem, and can simulate an ARP table given a simple LAN scenario.

ARP tables are need to move between layer 3 where IP addresses are used for identification and the link layer where MAC addresses are used for identification. The tables are self learning which means that as requests are made from users on the network, the table adds or updates the requesting device's IP and Mac Addresses. The self learning element of an ARP table makes it a plug and play implementation of the general forwarding tables which generally require some process to create their forwarding tables.   

##### I can explain CSMA/CA, how it differs from CSMA/CD, what problems it addresses, and how it solves them.

Contact avoidance is built on the concept of handling collisions by never letting them happen. Instead of contact detection, which makes it possible to fix collisions, contact avoidance is one step ahead by implementing a protocol that keeps the packets from crossing paths in the first place. Each node in the network follows this procedure to send packets:  
1) If the channel is idle, it transmits after the "difs" time.
2) Otherwise, the station chooses a random backoff time using binary exponential backoff. 
3) When the counter reaches 0, the host transmits the entire frame and waits for an ACK.
4) If the host gets an ACK, it goes to step two and keeps sending. If it does not get an ACK then it will go to step two with a larger binary exponential backoff. 
Using this protocol, a level of order is maintained that will greatly reduce collisions overall. 