### Lab Questions:

#### How did you solve the Chapters? Please copy and paste your winning strategy(s), and also explain it in English.

For chapter 1 I just threw in this line:
    self.send_message("Mayday!", "N")
which worked great.

For chapter 2, I misread the tools I was given and actually started implementing my own state variebles table. Once I figured it out I did this:
    while(self.message_queue):
        m = self.message_queue.pop()
        print(f"Msg on interface {m.interface}: {m.text}")
    
        self.state["received"][m.interface][0] += 1             //Tracking
        self.state["received"][m.interface][1] += int(m.text)   //Adding
        if(self.state["received"][m.interface][0] == 3): 
            print(f"Wants to send {self.state["received"][m.interface][1]} over {m.interface}")
            self.send_message(self.state["received"][m.interface][1], m.interface)
It is a pretty straight forward allocation of received data to tracking and adding lines. The added fields load the message and the tracking fields fire it. 

Include a section on Beta Testing Notes.

#### List any bugs you find. When you list the bugs, try to specify the exact conditions that causes them so I can reproduce them.
I encountered no bugs on these parts. 

#### Was there anything that you and others were consistently confused about? What would you do to solve that confusion?
Getting into python was tricky. Taking it slow and reading the instruction made it very clrear. 

#### Was there too much narrative? Not enough narrative?
Perfect amount. It helped frame the problem without getting too wordy. 

#### Was it fun?
Yes, it was a very nice change of pace.

#### Really, just let me know how I can improve the game.

I do not know if this is productive but getting it all set up was half the battle. I think that streamlining that would improve the game overall. 