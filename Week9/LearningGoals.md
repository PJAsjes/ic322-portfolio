## Learning Goals 

#### I can describe how Link State algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Link State algorithm.

In the link state algorithm, each node in the network broadcats its connection speeds to the entire network. The nodes relativity to other nodes can be thought of as vectors. Whenever a nodes position is updated it will let everyone know about its update. If this improves another nodes path to a destination, it will update its table and broadcast. get a better path to a node or finds out about a new node, then it will udate its table and notify. Dijkstra's Algorithm is used by Link State Algorithms to update distances. This may seem costly but it saves a lot of time when it comes to transmission becuase nodes already know the best path. Link state is better for handling a network that needs to transmit regularly but does not undergo a great deal of change to the network structure. 
So what happens when a situation changes the structure of a network. For example one router could be handling a large queue of packets that makes it slower to transmit then the rest of the network thinks. To solve this, nodes regularly transmit their speeds so that the network is always staying updated. 
 

#### I can describe how Distance Vector algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Distance Vector algorithm.

Distance Vector Algorithms are based on an iterative, asynchronous, and distributed system of updating the shortest distances. What that means is that information moves like gossip. Any change or creation of a node will result in that node telling its neighbors and they will in tern tell their neighbors until the enitre networks is updated. The algorithm iterates through all of the node and updates their tables for the optimal path then it self terminates (which is an advantage of Distance Vector). All the information in a given node is shared directly with its neighbor making it a distributed algorithm. Becuase each node is updating based on the entire autonomous system, the nodes do not need to update in "rounds" allowing it to be considered asynchronous. The nodes are regularly updating their neighbors and beong updated using the algorithm $d_{x}(y) = min_{c}c(x,v) + d_{v}(y). the maid function $d_{v}(y) is the cost of steps. x and y are the start and end nodes being measured. c() is a cost function givin a "starting from" state of v.
        when(Node A is updated){
            for(all of As neighbors){
                for(all of the table entries in the current neighbor's entry table){
                    if([distance from node A to the current neighbor] + [current neighbor's current entry] < node As path to the current 
                    neighbor's current entry){
                        update node A to the faster path
                    }
                    else if(A does not have curr neighbor's curr entry table entry){
                    
                    }
                }
            }
        }
This algorithm is applied and shared with all neighbors, then the node waits for a neighbor to share and make adjustments as needed. 