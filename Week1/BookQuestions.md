## Textbook Question:

#### R1: What is the difference between a host and an end system? List several different types of end systems. Is a Web server an end system?

Hosts and end systems are interchangable terms. **End Systems** include 
things like desktop computers, servers, and mobile devices (Textbook pg.
40). End Systems are are the begining and final destination of 
information where it is packaged and unpackaged at the highest level. 
**Hosts** (aka End systems) are generally broken up into clients and servers. 
Clients conceptually associate with the users of the internet. Clients 
tend to go with desktops, mobile devices, laptops kind of stuff. Servers 
are the bigger warehouse types of end systems. I think of this like the 
servers in Hopper Hall (servers) and my personal laptop (client). Web 
Servers are not an end state but are held in an end state.      

#### R4: List four access technologies. Classify each one as home access, enterprise access, or wide-area wireless access.

**DSL** is a modification of phone lines with the purpose of transmitting 
data using noise tones like a language. This can be used in home or 
enterprise access. **Coaxial Cable** is another type of Access Tech that 
caries data over long distances through wires directly to an end 
state. Routing cable to every home can be extremely expensive but it 
is an obvious (if innefficient) solution to the data transmission 
problem. **Ethernet** allows the internet to get right into a highly 
concentrated are and then disperse to a large group of users. It is 
used mainly in enterprise access for universities or companies. Wide 
Area access is solved by the wireless infarsturucture used for cellular 
devices. **4G/5G wireless services** provide a large area with access to 
data. 

#### R11:Suppose there is exactly one packet switch between a sending host and a receiving host. The transmission rates between the sending host and the switch and between the switch and the receiving host are R1 and R2 respectively. Assuming that the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet of length L? (Ignore queuing, propagation delay, and processing delay.)

The total end to end delay will be: L = (R1 * packet_size) + (R2 * 
packet_size)

#### R12: What advantage does a circuit-switched network have over a packet-switched network? What advantages does TDM have over FDM in a circuit-switched network?

**Circuit-switched** has the advantage of a reserved connection which 
alleviates the queueing delay. **TDM** enjoys the advantage of sole 
control of its slot. There is no additional noise over a TDMs 
transmission which means the system does not need to parse the from the 
connection. The disadvantage is the silent periods of a TDM connection.  

#### R13:Suppose users share a 2 Mbps link. Also suppose each user transmits continuously at 1 Mbps when transmitting, but each user transmits only 20 percent of the time. 

##### a. When circuit switching is used, how many users can be supported?

10 or fewer users can be supported over a circuit switched connection 
based on the math of a 1Mbps connection. 
    
##### b. For the remainder of this problem, suppose packet switching is used. Why will there be essentially no queuing delay before the link if two or fewer users transmit at the same time? Why will there be a queuing delay if three users transmit at the same time?

For two or fewer users the time to process the packet is so small that 
the time waiting in the queue for data to be proccessed is essentially 
non existant. Three users could start to create a queueing delay if all 
the users tried to send data at the exact same time but it would still be 
neglegable. 

##### c. Find the probability that a given user is transmitting.

There is a 0.2 that a user is transmitting.

##### d. Suppose now there are three users. Find the probability that at any given time, all three users are transmitting simultaneously. Find the fraction of time during which the queue grows.

The probability of all three transmitting is 1 in 125 or 0.008. 
Time * 0.008 is the frame of time in which the queue is growing. 

#### R14: Why will two ISPs at the same level of the hierarchy often peer with each other? How does an IXP earn money?

ISPs can consolidate their their overhead and speed up network 
connections by peering with one another. By sharing resources, ISPs cut 
down on the opperating costs of inferstracture needed to provide network 
connections to the millions of users interested in internet access. 
Users of the internet pay ISPs in order to gain access to the internet. 
Individuals do not have the resources to make a connection to the net so 
they pay ISPs in order to gain access. 

#### R18: A user can directly connect to a server through either long-range wireless or a twisted-pair cable for transmitting a 1500-bytes file. The transmission rates of the wireless and wired media are 2 and 100 Mbps, respectively. Assume that the propagation speed in air is 3 * 108 m/s, while the speed in the twisted pair is 2 * 108 m/s. If the user is located 1 km away from the server, what is the nodal delay when using each of the two technologies?

**Long-range wireless:**  
 1500 bytes         1 s           1 Km      1 s  
 ----------  *  ------------   +  ----  *  -----  =  0.00075  + 3.33 E -9 = 0.74 E-4 s  
    1          2 E 6 bytes        1      3 E 8  

**twisted-pair cable:**  
1500 bytes         1 s            1 Km      1 s 
----------  *  -------------   +  ----  *  -----  = 1.5 E -5 +   5 E -9  = 1.5005 E-5 s
    1          100 E 6 bytes       1       2 E 8

#### R19: Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has three links, of rates R1 = 500 kbps, R2 = 2 Mbps, and R3 = 1 Mbps.

##### a. Assuming no other traffic in the network, what is the throughput for the file transfer?
    
The transmission is held up by the slowest link. This means that the 
becuase R1 is the slowest linkl, the throughput is 500 kbps.

##### b. Suppose the file is 4 million bytes. Dividing the file size by the throughput, roughly how long will it take to transfer the file to Host B?
    
4,000,000 / 500 kbps = 8 seconds

##### c. Repeat (a) and (b), but now with R2 reduced to 100 kbps.
    
This would cause R2 to be the bottle neck for the throughput and it would become 
100 kbps. 4,000,000 / 100,000 bps = 40 Seconds. 