## Learning Goals

#### I can explain the role that the network core plays vs the network edge.

The Network edge is like the crust of the internet which a majority of users 
interact with on regular basis. The edge consists of smartphones, laptops, and 
desktops to name a few forms. The edge is where the data of the internet is abstraced 
to a consumable form. The network core is what makes the internet a network. It 
consists of all the physical "roads" and conventions that are neccessary to convey 
the mass amounts of information accross the world at light speed. The core consists 
of the wires, servers and ISPs that keep the blood (data) of the internet flowing. 
    
#### I can compare different physical technologies used in access networks, including dial-up, DSL, cable, fiber, and wireless.

Access networks are roads on which the data of the internet travel. Dial-up is a 
carry over from when phone companies were on the forefront of passing information. 
Dial-up makes sense from the perspective of using existing inferstrtucure to get data 
to users. Dial-up is however, a very weak approach to efficient transmission of data. 
Sound cannot compete with current when it comes to vollume of data transmitted over 
time. DSL is one step up from the dial up approach. DSL can still benefit from 
existing architecture of phone lines but has the advantage of using electricity 
instead of sound to transmit. Cable is a definite step up from DSL from a physics 
standpoint. Fiber is yet another optimization of the Cable model. Fiber is simply 
pushing the envelope on what the best possible meterial for data transmission. Both 
cable and fiber are victims of the fact that they have to be installed from scratch. 
Wireless reduces some of the strain of installation by cutting out the last 10 
kelometers of inferstructure by placing a large umberella of service on an area from 
a tower. Wireless chokes data flow slightly but is a definite balance between
improving data flow and avoiding constructing insane amounts of new physical 
connections.    

#### I can use queuing, transmission, and propagation delays to calculate total packet delay, I can describe the underlying causes for these delays, and I can propose ways to improve packet delay on a network.

When data is transmitted for from one system to another it undergoes several 
delays caused by various things. Routers can generally only manage one packet of 
data at a time. If a packet of data arrives when the router is already handling 
something then there will be a delay for the second packet whit the router handles
the first packet. This is called QUEUING DELAY. Optimizing the router to handle data faster will reduce Queuing delays.

When the router has received the data it will take the location from the header. 
The router will then need to transcribe the data down to the wire that will take it 
to its final destination. This is called TRANSMISSION DELAY. Optomizing the 
algorothms used to transmit data in the most efficient way will reduce transmission 
delay.

The time that it takes the data to run over the physical wire is ussually close 
to the speed of light but still takes time which needs to be accounted for. This
is called PROPAGATION DELAY. Better wires and more efficient forms of transmission
will minimize the Propogation delay.   

#### I can describe the differences between packet-switched networks and circuit-switched networks.
    
Packet Switched Networks are described in the textbook as the Trucking Industry. 
They run through the network of "Highways" to reach their destination.
A circuit-switched network is one that creates and holds a direct line between 
the sender and receiver. Data is transfered at a predefined and constant rate. 

A packet switched network sends data of preprescribed size using TCP and IP over an 
ISP. The packet switched has the advantage of using all of the ISPs resources where a 
circuit switched network merely uses a single direct connection to communicate. 
Ongoing communication can be The deffect of packet switching is that if there are a 
large number of users on the netweork then there is a queueing delay as packets wait 
on eachother to get through the router. Circuit switching does also waste time when 
no active communication is taking place becuase the entire line is still devoted that 
end to end link even during silent periods.   

#### I can describe how to create multiple channels in a single medium using FDM and TDM.

FDM transmits the data off of a predefined bandwidth. Additional channels can be added to FDM by using a larger bandwidth. In contrast, TDM uses windows of time to transmit data. Timing uses frames of time to transmit. These two methods of transmiting can be used simultaniously almost creating a 3D transmission. FDM using height and TDM using length of the transmission.  

#### I can describe the hierarchy of ISPs and how ISPs at different or similar levels interact.

Users enter into the network by through an access ISP. The access ISP pays for access to the inferstracture built by regional ISPs. Regional ISPs sometimes use peering to share resources but are ussualls limited in their ability to access the world wide web. Tier 1 ISPs provide the big network access need to make the global network work.  

#### I can explain how encapsulation is used to implement the layered model of the Internet.

Any information that you (as a user) would like to get off of the internet resides in 
a server somewhere or at another users end state (or persoanl device). Getting to 
that end state and getting it back requires a process of packaging your request and 
getting the packaged information back. The internet modeled in layers describes every 
part of the transmission as a different layer. Each layer adds either mobility or 
specificity to your request and the respons. As data moves through the layers it 
gains all the neccessary elements to reach its destination. Encapsulation helps us 
visualize these layers. All data moves over the physical layer becuase that is 
neccessary for the actual 1s and 0s to be conveyed. From there, each layer uses its 
own kind of addressing to get the data over that layers hurdle. Once each layer uses 
its protocals to get the next step, the data will have reached its destination. 
Encapsulation helps to visualize this as each layer solves a more complex and specific
problem by building on the preceding layers. The Application layer only has to take
care of a relativly small delivery problem of droping it on the servers doorstep 
becuase every  other layer has taken the data all the way to the doorstep. That is why
encapsulation helps describe the movement of information over the  internet.      