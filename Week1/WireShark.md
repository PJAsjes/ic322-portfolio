## Wireshark Lab Answers:

#### 1. Which of the following protocols are shown as appearing (i.e., are listed in the Wireshark “protocol” column) in your trace file: TCP, QUIC, HTTP, DNS, UDP, TLSv1.2?

>TCP, TLSv1.2, and DNS were the protocals that appeared in my wireshark. 

#### 2. How long did it take from when the HTTP GET message was sent until the HTTP OK reply was received? (By default, the value of the Time column in the packet-listing window is the amount of time, in seconds, since Wireshark tracing began.)  (If you want to display the Time field in time-of-day format, select the Wireshark View pull down menu, then select Time Display Format, then select Time-of-day.)

>The GET was sent at 8.478292 seconds and the OK was received at 8.500013 seconds so it took 0.021721 seconds. 

#### 4. What type of web browser issued the HTTP request?

>I did it on Firefox. "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/116.0\r\n."

#### 5. What is the destination port number to which this HTTP request is being sent?

>Port 80