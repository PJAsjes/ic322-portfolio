### Lab intro
The following is my submission for the week 12 Lab which was a protocol pioneer chapter geared towards link layer implementation of anti collision protocols. My partner was Adley Louissaint. 

### Include your client and server strategy code.

    def server_strategy(self):
   
    self.set_display(f"Queued:{len(self.from_layer_3())}")

    # Handle messages received from our interfaces, destined for layer 3
    while(self.message_queue):
            m = self.message_queue.pop()
            #print(f"--- Server {self.id}: Msg on interface {m.interface}: Tick {self.world.get_current_tick()} ---\n{m.text}\n------------------")
           
            # If this message belongs to us, send it to layer 3
            if parse_message(m.text).get("Destination") == self.id:
                self.to_layer_3(m.text)


    # Handle messages from layer 3, destined for the interfaces
    # Each entity has a single interface in this scenario. Figure out what the interface is.
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0]
    if not self.interface_sending(selected_interface):
        if len(self.from_layer_3()) > 0:
            if self.current_tick()%16 == ((int(self.id)-2)*4-2) or self.current_tick()%16 == ((int(self.id)+2)*4-2):
                if not self.interface_receiving(selected_interface):
                    msg_text = self.from_layer_3().pop()
                    print(f"server {self.id}: Attempting send to: " + msg_text)
                    self.send_message(msg_text, selected_interface)
                else:
                    self.cancel_interface(selected_interface)


    def client_strategy(self):
        ##print(f"{self.current_tick()} --This is the current tick")
        self.set_display(f"Queued:{len(self.from_layer_3())}")

        # Handle messages received from our interfaces, destined for layer 3
        while(self.message_queue):
                m = self.message_queue.pop()

                # If this message belongs to us, send it to layer 3
                if parse_message(m.text).get("Destination") == self.id:
                    self.to_layer_3(m.text)


        # Handle messages from layer 3, destined for the interfaces
        # Each entity has a single interface in this scenario. Figure out what the interface is.
        connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
        selected_interface = connected_interfaces[0]
        if not self.interface_sending(selected_interface):
            if len(self.from_layer_3()) > 0:
                if self.current_tick()%16 == ((int(self.id)-1)*4) or self.current_tick()%16 == ((int(self.id)+1)*4):
                    if not self.interface_receiving(selected_interface):
                        msg_text = self.from_layer_3().pop()
                        dest = msg_text.split(",")[1].split(":")[1]
                        print(f"client {self.id}: Attempting send to: {dest} : current tick is {self.current_tick()%16}")
                        self.send_message(msg_text, selected_interface)
                    else:
                        self.cancel_interface(selected_interface)

            if self.interface_receiving(selected_interface):
                print("i canceled the send")
                self.cancel_interface(selected_interface)
    ```


### Explain your strategies in English.
The strategy was to alot a sending time to each of the nodes in the network where each node could ensure a clear network to send over. We also tried to implement a 2/1 ratio for the clients/servers respectivly becuase it seemed that clients received twice the packets of the servers. 

### What was your maximum steady-state success rate (after 300 or so ticks?)
0.20

### Evaluate your strategy. Is it good? Why or why not?
The strategy was good but the implementation was not all there. 

### Are there any strategies you's like to implement, but you don't know how?
I would like to implement a "Listen and React" style of protocol where nodes take advantage of the garbage transmission to update a dynamic table of the networks current state to optimize their sending time. 

### Any other comments?
This was a very fun lab. I enjoyed the "as creative as you can be format" which allowed for a very open exploration of the week's topics. 