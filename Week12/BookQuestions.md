## Book Questions:

#### R4, Suppose two nodes start to transmit at the same time a packet of length L over a broadcast channel of rate R. Denote the propagation delay between the two nodes as dprop. Will there be a collision if dprop 6 L/R? Why or why not?
There will be a collision if the ratio of packet length excedes the broadcast rate by a factor of 6 as the two packets will cross all over eachother. 

#### R5, In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?
For slotted Aloha:
-When only one node is sending, it sends as fast as it can.
-The protocol is decentralized; that is, there is no master node that represents a single point of failure for the network.
-It has very little overhead to run becuase it is simple.

#### R6 In CSMA/CD, after the fifth collision, what is the probability that a node chooses K = 4? The result K = 4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet?
1/32 becuase 0 to 2^5 is one in thirty-two which corresponds to a delay of 204.8 seconds.

#### P1, Suppose the information content of a packet is the bit pattern 1010 0111 0101 1001 and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used.

This would make a 4 by 4 matrix which should look as follows: 
    1 0 1 0 - 0
    0 1 1 1 - 1
    0 1 0 1 - 0
    1 0 0 1 - 0
    | | | |   |
    0 0 1 1 - An odd sum makes 1.
therefor: 0100 0011 1

#### P3, Suppose the information portion of a packet contains six bytes consisting of the 8-bit unsigned binary ASCII representation of string “CHKSUM”; compute the Internet checksum for this data.
01000011 +
01001000 +
01001011 +
01010011 +
01010101 +
01001101 =
11111111 which goes to 00000000 in ones compliment.

#### P6, Rework the previous problem, but suppose that D has the value
##### a. 01101010101.
##### b. 11111010101.
##### c. 10001100001.

#### P11, Suppose four active nodes—nodes A, B, C and D—are competing for access to a channel using slotted ALOHA. Assume each node has an infinite number of packets to send. Each node attempts to transmit in each slot with probability p. The first slot is numbered slot 1, the second slot is numbered slot 2, and so on. 
##### a. What is the probability that node A succeeds for the first time in slot 4?
    (1-p)^3 * p * (1-p)^3
##### b. What is the probability that some node (either A, B, C or D) succeeds in slot 5?
    p∗4∗1/4
##### c. What is the probability that the first success occurs in slot 4?
    4 ∗ ((1−p)^3)^4 ∗ p
##### d. What is the efficiency of this four-node system?
    4p(1 − p)^3
#### P13 Consider a broadcast channel with N nodes and a transmission rate of R bps. Suppose the broadcast channel uses polling (with an additional polling node) for multiple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmit (that is, the polling delay) is dpoll. Suppose that within a polling round, a given node is allowed to transmit at most Q bits. What is the maximum throughput of the broadcast channel?
This will be the max bits divided by transmission rate/max bits + d_poll:
        Q
    ----------
    R
    - + d_poll
    Q