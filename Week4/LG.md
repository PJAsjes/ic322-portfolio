## Learning Goals

#### I can explain how TCP and UDP multiplex messages between processes using sockets.

Multiplexing is the process of packaging and delivering information in the transport layer. TCP and UDP protocals multiplex messages in unique ways. UDP uses a two-tuple to identify its port. This means that multiple connections made over UDP use the same port at the receiver. TCP on the other hand, for every server there is a "welcoming" socket. The welcoming socket handles establishing a secure connection before assigning the user to a requested port number. Any TCP multiplexing will contain (1) the source port number in the segment, (2) the IP address of the source host, (3) the destination port number in the segment, and (4) its own IP address. This creates a stronger connection. 

#### I can explain the difference between TCP and UDP, including the services they provide and scenarios each is better suited to.

**TCP (Transmission Controled Protocal)** is connection oriented. This means that TCP is focused on the secure and succesful delivery of packages. TCP is the best way to send data that all needs to get to the destination garenteed and secure. Sending banking information would be over TCP becuase you need it to be secure and you need every packet of the information. The **UDP (User Datagram Protocol)** is unreliable data transfer. It can be better for things like streaming where the user will not notice a couple of packages missing and security is less important than speed. 


#### I can explain how and why the following mechanisms are used and which are used in TCP: sequence numbers, duplicate ACKs, timers, pipelining, go-back-N, selective repeat, sliding window.

**Sequence numbers:** are used to confirm that packetes are delivered in order. If a receiver gets SEG1 then SEG3 then it knows that SEG2 was missed and can notify the sender that packets were lost.
    
**Duplicate ACKs** are sent by the receiver to notify the sender that the next sequential packet was not sent. This is an efficient way to relay that packets have been missed.
**Timers** can be started when a package or packages are sent in order to give the receiver time to receive a missing packet before sending it again.
    
**Pipelining** is the process of sending packets before previous ones are aknowledged in order to speed up the process.
    
**go-back-n** uses a sliding window to set when missed packets will be resent. The window is of size N is used tolimit the pipelining. A single timer is used to tell when the resend should begin, however the sender will not send more than N packets that need ACKs. Go-back-n discards any out of order packets on the receiver side.  
    
**Selective repeat** tracks a timer for every sent packet at the sender side. Each out of order packet is also held until the missing packets can fill in the difference. Selective repeat is the most individual packet focused.  
    
#### I can explain how these mechanisms are used to solve dropped packets, out-of-order packets, corrupted/dropped acknowledgements, and duplicate packets/acknowledgements.

**Dropped packets** are tracked by **duplicate ACKs** and **sequence numbers**. If the sequence are received out of order than the receiver knows that something has probobly been dropped. If a duplicate ACK is sent then the sender is made aware that packets were received out of order. A **timer** then can be used by the sender to see if the packets were just sent out of order or if it was completly dropped. Senders and receivers handle duplicate ACKs/packets by disreguarding duplicate packets based on the sequence numbers. Duplicate ACKs is the indicator that they may need to be re-sent.   