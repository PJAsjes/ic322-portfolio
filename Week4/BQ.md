## Book Questions

##### R5,  Why is it that voice and video traffic is often sent over TCP rather than UDP in today's internet?

UDP does not have the security that most users are looking for. While it is time sensitive, users generally prefer their conversations being confidential over speed.

##### R7, Suppose a process in Host C has a UDP socket with port number 6789. Suppose both Host A and Host B each send a UDP segment to Host C with destination port number 6789. Will both of these segments be directed to the same socket at Host C? If so, how will the process at Host C know that these two segments originated from two different hosts?

Both connections will go to the same socket. However, there is no clear distinction between the two connections sources. This is one of the less desireble aspects of UDP. The server will have to look at the connection information in order to find out what the source IP actually is.  

##### R8, Suppose that a Web server runs in Host C on port 80. Suppose this Web server uses persistent connections, and is currently receiving requests from two different Hosts, A and B. Are all of the requests being sent through the same socket at Host C? If they are being passed through different sockets, do both of the sockets have port 80? Discuss and explain.

This connection will take place over TCP. This means that a four-tuple is used to create the connection. It will start by handling both connections on a welcoming socket. Each connection is then assigned a unique socket to port 80 that contains the source port and IP address. 

##### R9, In our rdt protocols, why did we need to introduce sequence numbers?


##### R10, In our rdt protocols, why did we need to introduce timers?

##### R11, Suppose that the roundtrip delay between sender and receiver is constant and known to the sender. Would a timer still be necessary in protocol rdt 3.0, assuming that packets can be lost? Explain.

##### R15 Suppose Host A sends two TCP segments back to back to Host B over a TCP connection. The first segment has sequence number 90; the second has sequence number 110.
###### a. How much data is in the first segment?
20
###### b. Suppose that the first segment is lost but the second segment arrives at B. In the acknowledgment that Host B sends to Host A, what will be the acknowledgment number?
90

##### P3 UDP and TCP use 1s complement for their checksums. Suppose you have the following three 8-bit bytes: 01010011, 01100110, 01110100. What is the 1s complement of the sum of these 8-bit bytes? (Note that although UDP and TCP use 16-bit words in computing the checksum, for this problem you are being asked to consider 8-bit sums.) Show all work. Why is it that UDP takes the 1s complement of the sum; that is, why not just use the sum? With the 1s complement scheme, how does the receiver detect errors? Is it possible that a 1-bit error will go undetected? How about a 2-bit error?