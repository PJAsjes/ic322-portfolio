## Book Questions

#### R11. What is the role of a packet scheduler at the output port of a router?

It lays out the order that the packets will be sent over the wire. 

#### R12. a. What is a drop-tail policy? b. What are AQM algorithms? c. Name one of the most widely studied and implemented AQM algorithms and explain how it works.

a. A drop tail policy is the way that a router handles packets when the buffer is full. 
b. Active Queue Managment is pretty much exactly what it sounds like. It is the policies that are used to drop and retain packets while performing active congestion control. 
c. The Random Early Detection Algorithm or RED uses an average queue length to calculate wether the packet should be enqueued or dropped. If the avererage is less than the min queue value it will be held but if the average is greater than it will atomatically drom the packet. If the average is inbetween the max and min then it will randomly decide to keep or drom the packet.  

#### R13. What is HOL blocking? Does it occur in input ports or output ports? 

HOL blockeing is when a packet that has a free output port is stuck behind packets that are in a queue for a congested output port. This is like when you are in the right turn lane on a red light trying to do a right on red but the car infront of you is going straight so you have to wait on the light. 

#### R16, What is an essential different between RR and WFQ packet scheduling? Is there a case (Hint: Consider the WFQ weights) where RR and WFQ will behave exactly the same?

WQF and round robin give different amounts of service to the queues based on how they prioritize the packets based on their classes. They act the same when the packets they receive are weighted exactly the same.  

#### R18, What field in the IP header can be used to ensure that a packet is forwarded through no more than N routers?

time-to-live

#### R21 How many IP addresses does a router have?

Routers have two IP addresses. The WAN which faces the external network where-as the LAN faces all networks internal to the network. 

#### P4, Consider the switch shown below. Suppose that all datagrams have the same fixed length, that the switch operates in a slotted, synchronous manner, and that in one time slot a datagram can be transferred from an input port to an output port. The switch fabric is a crossbar so that at most one datagram can be transferred to a given output port in a time slot, but different output ports can receive datagrams from different input ports in a single time slot. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?

The best case scinario is 3 steps. There is never a time that you can send more than two datagrams in one time slot. 


#### P5, Suppose that the WEQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.
#### a. Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights? (For round robin scheduling, a natural sequence is 123123123 . . .).

It has to go 11231123 in order to meet the policy. 

#### b. Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?
112112 would work. 

#### P8, Consider a datagram network using 32-bit host addresses. Suppose a router has four links, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows:
######    Destination Address Range **Link Interface** 
    11100000 00000000 00000000 00000000
                    through                     0
    11100000 00111111 11111111 11111111

    11100000 01000000 00000000 00000000
                    through                     1
    11100000 01000000 11111111 11111111

    11100000 01000001 00000000 00000000
                    through                     2
    11100001 01111111 11111111 11111111
    
                    otherwise                   3

###### a. Provide a forwarding table that has five entries, uses longest prefix matching, and forwards packets to the correct link interfaces.

    11100000 00        -> 0
    11100000 01000000  -> 1
    11100000 01        -> 2
    11100001 1         -> 3
    OTHERWISE          -> 3


###### b. Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses:
    11001000 10010001 01010001 01010101 -> 3
    11100001 01000000 11000011 00111100 -> 2
    11100001 10000000 00010001 01110111 -> 3

#### P9, Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table:
    Prefix Match    Interface
    00              0   
    010             1
    011             2
    10              2
    11              3
#### For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.

00000000 - 00111111   64 addresses in range  2^6 = 64
01000000 - 01011111   32 addresses in range  2^5 = 32
01100000 - 10111111   96 addresses in range  2^5 + 2^6 = 96
11000000 - 11111111   64 addresses in range  2^6 = 64

#### P11 Consider a router that interconnects three subnets: Subnet 1, Subnet 2, and Subnet 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is to support at least 90 interfaces, and Subnet 3 is to support at least 12 interfaces. Provide three network addresses (of the form a.b.c.d/x) that satisfy these constraints.

60 = 0011 1100 6 bits needed
Subnet 1: 223.1.17.63/26

90 = 0101 1010 7 bits needed
Subnet 2: 223.1.17.191/25

12 = 0000 1010 4 bits needed
Subnet 3: 223.1.17.207/28