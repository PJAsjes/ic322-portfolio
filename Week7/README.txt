Forwarding (Data Plane) vs Routing (Control Plane)
-> Forwarding is in the router, routing is across routers. 
Forwarding Table
-> The forwarding table translates a packet field to the correct output link.
Longest Prefix Matching
-> send the packet to the best match
Switching Fabrics
    Memory vs Interconnection Network vs Bus
    Input Queuing and How Switching Fabric Affects These
Output Port Processing
    Queueing - Why It Occurs, Packet Loss
Active Queue Management Algorithms
    FIFO
    Priority Queue
    Round Robin
    Weighted Fair Queueing
Buffer Sizes and Their Implications
IPv4
    Datagram Format
    Addressing
    Subnets, Subnet Mask, CIDR Notation
    Hierarchical Addressing and Route Aggregation
    Obtaining a Block of Addresses
    DHCP