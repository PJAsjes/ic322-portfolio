Write Up:
This lab very much solidifyed my understanding of forwarding tables and the role they play in progressing data through the network
layer. The only issues I ran into throughout the lab were with Python and never with the concepts. 
In my main method, I collected the binary string and the sent it to a helper method to be compared to the provided routeing table. The helping
method is designed to return an integer value for the table entry that the user input best associates with. The main will then print out the port 
that associates with that numeric value. I ran into all of my problems in comparing the values and manipulating the strings in python to compare 
them. 
The program now correctly routes the binary IP addresses based on the routing table provided. 

All test cases work!

Provideded files:

Test case:

00000001000000010000000100000001
00000001000000000110010000000001
11110000111100001111000011110000
01100101000001010000011100100001
01100101000001010000011100101010
11011111000001010110001101100101
11011111000001000110001101100101
11010001000001010110001101100101

Fa 0/0
Fa 0/1
Gi 3/1
Fa 0/7
Fa 0/6
Fa 2/6
Gi 1/1
Gi 3/1
