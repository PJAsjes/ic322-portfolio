import sys

def LongestPrefixMatch(input_data_list):
    ## this is a list of strings so far
    routing_table_num = list("1.0.100.0/24 1.99.0.0/20 8.0.0.0/27 8.8.8.0/24 126.2.3.0/20 8.8.8.16/30 237.1.1.0/30 237.2.0.0/16 99.31.4.0/22 99.0.0.0/8 101.5.0.0/16 223.0.0.0/8 101.5.7.32/30 99.0.0.0/20 101.0.0.0/8 101.42.3.0/24 223.4.0.0/16".split(" "))
    routing_table_binary = [[0 for _ in range(4)] for _ in range(18)]
    ##print(routing_table_binary)

    for i in range(0, len(routing_table_num)):
        route_parsed = list(routing_table_num[i].split("."))
        for j in range(0,3):        
            temp = bin(int((route_parsed[j])))
            routing_table_binary[i][j] = temp.lstrip("0b")
            while(len(routing_table_binary[i][j]) < 8):
                routing_table_binary[i][j] = "0" + routing_table_binary[i][j]
        routing_table_binary[i][3] = route_parsed[3]
        

    best_match = 17
    best_match_str = 0
    for i in range(0,17):
        current_match_strength = 0
        routing_comp = routing_table_binary[i][0] + routing_table_binary[i][1] + routing_table_binary[i][2] + routing_table_binary[i][3]
        ##print(routing_comp + "<= compared to")
        for j in range(0,25):
            if(input_data_list[j] != routing_comp[j]):
                break
            current_match_strength += 1
        ##print(current_match_strength)
        if(current_match_strength > best_match_str):
            best_match = i
            best_match_str = current_match_strength
        print("CMS:")
        print(current_match_strength)
        print("BMS:")
        print(best_match)
    return best_match


input_data = input()
input_data_list = list(input_data)

routing_table_letters = list("Fa 0/1  Fa 2/1  Fa 0/2  Fa 2/2  Fa 0/3  Fa 2/3  Fa 0/4  Fa 2/4  Fa 0/5  Fa 2/5  Fa 0/6  Fa 2/6  Fa 0/7  Fa 2/7  Gi 1/0  Gi 3/0  Gi 1/1  Gi 3/1".split("  "))

best_match = LongestPrefixMatch(input_data_list)

print(routing_table_letters[int(best_match)])
