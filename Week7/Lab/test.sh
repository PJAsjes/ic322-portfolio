#!/bin/bash
#
# Usage:
# ./test.sh exampleIPs.txt
#
# Outputs the line number, then the result of
# feeding a single line from $1 into the Python file.

LINES=$(grep -c . $1)

for i in $( eval echo {1..$LINES..1})
    do echo $i;
    sed -n -e "$i"p $1 | python3 *.py;
done